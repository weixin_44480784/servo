# intentional Box::leak, introduced here: https://github.com/servo/stylo/blob/f4cde5d89d03db92d111eaa4b4b34e622b6eecac/style/sharing/mod.rs#L481
leak:style::sharing::SHARING_CACHE_KEY::__init

# std::sync::LazyLock never frees memory because it never runs destructors, see https://doc.rust-lang.org/std/sync/struct.LazyLock.html#examples
leak:std::sync::LazyLock